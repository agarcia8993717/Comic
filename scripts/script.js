<script type="module">
  import { Toast } from 'bootstrap.esm.min.js'

  Array.from(document.querySelectorAll('.toast'))
    .forEach(toastNode ={">"} new Toast(toastNode))
</script>

const miTitulo = document.querySelector("h1");
let nombredelaVariable = document.querySelector("h1");

miTitulo.textContent = "¡Hola!";

let saludo = "¡Hola a todos, bienvenidos a nuestro proyecto!";

function saludo() {
  alert("¡Hola a todos, bienvenidos a nuestro proyecto!");
}

let nombreDeLaVariable = "Nuestra Web";
nombreDeLaVariable = "Proyecto";
nombreDeLaVariable;

let miBoton = document.querySelector("button");
let miTitulo = document.querySelector("h1");

function estableceNombreUsuario() {
  let miNombre = prompt("Por favor, ingresa tu nombre.");
  localStorage.setItem("nombre", miNombre);
  miTitulo.textContent = "La pagina es buena!," + miNombre;
}

if (!localStorage.getItem("nombre")) {
  estableceNombreUsuario();
} else {
  let nombreAlmacenado = localStorage.getItem("nombre");
  miTitulo.textContent = "La pagina es buena!," + nombreAlmacenado;
}

miBoton.onclick = function () {
  estableceNombreUsuario();
};

function estableceNombreUsuario() {
  let miNombre = prompt("Introduzca su nombre.");
  if (!miNombre) {
    estableceNombreUsuario();
  } else {
    localStorage.setItem("nombre", miNombre);
    miTitulo.innerHTML = "La pagina es buena!, " + miNombre;
  }
}

// Script para completar nuestra pagina 